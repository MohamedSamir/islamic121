$("document").ready(function(){
	$("#from,#to").change(function() {
		$("#billing").dataTable().fnDestroy();
		var from = $("#from").val();
		var to = $("#to").val();
		$('#billing').dataTable({
			"ajax" : "index.php?r=report/getAllStudentsBillingReport&from="+from+"&to="+to
		});

	});
	$("#from").trigger("change");
});

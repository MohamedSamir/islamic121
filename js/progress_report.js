$("document").ready(function(){
	
	$("#from,#to,#student").change(function() {
		$("#datatable").dataTable().fnDestroy();
		var from = $("#from").val()+" 00:00:00";
		var to = $("#to").val()+" 00:00:00";
		var id = $("#student").val();
		//alert("index.php?r=Report/getLessons&from="+from+"&to="+to+"&id="+id);
		//alert("index.php?r=Treasury/getTransactions&from="+from+"&to="+to+"&currency="+currency);
		$('#datatable').dataTable({
			"aLengthMenu": [[100, 200, 300], [100, 200, 300]],
	        "iDisplayLength": 100,
	        "iDisplayStart" : 0,
			"aoColumns": [
            { "mData": "date" },  // for User Detail
            { "mData": "hefz_from" },
            { "mData": "hefz_to" },
            { "mData": "front_from" },
            { "mData": "front_to" },
            { "mData": "back_from" },
            { "mData": "back_to" },
            { "mData": "grade" },
            { "mData": "notes" },
        ],
			"ajax" : "index.php?r=Report/getLessons&from="+from+"&to="+to+"&id="+id
		});

	});

	$("#from").change();
});

<?php

class MailBoxController extends Controller {

	public function actionMessage() {
		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			$message = Message::model() -> find("id = :id", array(":id" => $id));
			if (Yii::app() -> user -> type == "Student")
				$type = 0;
			else if (Yii::app() -> user -> type == "Teacher")
				$type = 1;
			else if (Yii::app() -> user -> type == "Admin")
				$type = 2;
			if ($message -> from_user_type == 0)
				$user = Student::model() -> find("id = :id", array(":id" => $message -> from_user_id));
			if ($message -> from_user_type == 1)
				$user = Teacher::model() -> find("id = :id", array(":id" => $message -> from_user_id));
			if ($message -> from_user_type == 2)
				$user = Admin::model() -> find("id = :id", array(":id" => $message -> from_user_id));

			$messageReceiver = MessageReceivers::model() -> find("message_id = :id and to_user_id = :uid and to_user_type = :type", array(":id" => $id, ":uid" => Yii::app() -> user -> id, ":type" => $type));

			if (isset($message -> broadcast) && !$messageReceiver) {
				/*echo($message -> broadcast);

				 $msg_bridge = new MessageReceivers;
				 $msg_bridge -> message_id = $message -> id;
				 $msg_bridge -> to_user_id = Yii::app() -> user -> id;
				 $msg_bridge -> to_user_type = $type;
				 $msg_bridge -> is_read = 1;
				 $msg_bridge -> is_hide = 0;
				 $msg_bridge -> is_favourite = 0;
				 $msg_bridge -> is_important = 0;
				 $message_error = "";
				 if(!$msg_bridge -> save()){
				 foreach ($msg_bridge->getErrors() as $key => $value) {
				 foreach ($value as $error) {
				 $message_error .= $error . " ";
				 }
				 }
				 echo $message_error;
				 }*/
			} else {

				if ($messageReceiver) {
					$messageReceiver -> is_read = 1;
					$messageReceiver -> save();
				}
			}
			$this -> render("message", array("message" => $message, "sender" => $user));
		}

	}

	public function actionSent() {
		$type = 0;
		if (Yii::app() -> user -> type == "Student")
			$type = 0;
		else if (Yii::app() -> user -> type == "Teacher")
			$type = 1;
		else if (Yii::app() -> user -> type == "Admin")
			$type = 2;

		$messages = Message::model() -> findAll("from_user_id = " . Yii::app() -> user -> id . " and from_user_type = " . $type);
		$receivers = array();
		$i = 0;
		foreach ($messages as $msg) {
			$receiversInfo = MessageReceivers::model() -> findAll("message_id = " . $msg -> id);
			$receivers[] = array();
			/*if ($msg -> broadcast == 1)
				$receivers[$i][] = "All Students";
			else if ($msg -> broadcast == 2)
				$receivers[$i][] = "All Teachers";
			else if ($msg -> broadcast == 3)
				$receivers[$i][] = "All Supervisors";
			else if ($msg -> broadcast == 4)
				$receivers[$i][] = "All Admins";
			else if ($msg -> broadcast == 5)
				$receivers[$i][] = "All Users";
			*/
			foreach ($receiversInfo as $receiver) {
				//echo $i;

				//$receivers[$i][] = $receiver->
				if ($receiver -> to_user_type == 0) {

					$user = Student::model() -> find("id = " . $receiver -> to_user_id);
					$receivers[$i][] = $user -> first_name . " " . $user -> last_name;
					//	$i++;
				} else if ($receiver -> to_user_type == 1) {
					$user = Teacher::model() -> find("id = " . $receiver -> to_user_id);
					$receivers[$i][] = $user -> first_name . " " . $user -> last_name;
					//	$i++;
				} else if ($receiver -> to_user_type == 2) {
					$user = Admin::model() -> find("id = " . $receiver -> to_user_id);
					$receivers[$i][] = $user -> first_name . " " . $user -> last_name;
					//	$i++;
				}

			}
			$i++;
		}

		$this -> render("sent", array("allMsgs" => $messages, "receivers" => $receivers));

	}

	public static function getUnReadMessage() {
		$type = 0;
		if (Yii::app() -> user -> type == "Student")
			$type = 0;
		else if (Yii::app() -> user -> type == "Teacher")
			$type = 1;
		else if (Yii::app() -> user -> type == "Admin")
			$type = 2;

		//$allMsgs = MessageUserBridge::model() -> findAll("user_type = :type and user_id = :id", array(":type" => $type  ,  ":id"=> Yii::app()->user->id));

		$criteria = new CDbCriteria( array('order' => 'date DESC'));
		$criteria -> compare('is_read', 0, false);
		$criteria -> compare('to_user_type', $type, false);
		$criteria -> compare('to_user_id', Yii::app() -> user -> id, false);
		//$criteria -> compare('broadcast_type', 0, false);

		//$criteria2 = new CDbCriteria;
		//$criteria2 -> addSearchCondition("`message`.`broadcast`", $type + 1, false);
		//$criteria2 -> addSearchCondition("`message`.`broadcast`", 5, false, "OR");

		//$criteria -> mergeWith($criteria2, 'OR');

		//echo $criteria->getText().'<br/>';
		$allMsgs = MessageReceivers::model() -> with("message") -> findAll($criteria);
		return $allMsgs;
	}

	public function actionInbox() {
		$type = 0;
		if (Yii::app() -> user -> type == "Student")
			$type = 0;
		else if (Yii::app() -> user -> type == "Teacher")
			$type = 1;
		else if (Yii::app() -> user -> type == "Admin")
			$type = 2;

		//$allMsgs = MessageUserBridge::model() -> findAll("user_type = :type and user_id = :id", array(":type" => $type  ,  ":id"=> Yii::app()->user->id));

		$criteria = new CDbCriteria( array('order' => 'date DESC'));
		$criteria -> compare('to_user_type', $type, false);
		$criteria -> compare('to_user_id', Yii::app() -> user -> id, false);
		//$criteria -> compare('broadcast_type', 0, false);

		//$criteria2 = new CDbCriteria;
		//$criteria2 -> addSearchCondition("`message`.`broadcast`", $type + 1, false);
		//$criteria2 -> addSearchCondition("`message`.`broadcast`", 5, false, "OR");

		//$criteria -> mergeWith($criteria2, 'OR');

		//echo $criteria->getText().'<br/>';
		$allMsgs = MessageReceivers::model() -> with("message") -> findAll($criteria);

		//var_dump($allMsgs[0]->message);
		$senders_names = array();
		$i = 0;
		foreach ($allMsgs as $msg) {
			$message = $msg -> message;
			if ($message -> from_user_type == 0) {
				$sender = Student::model() -> find("id = " . $message -> from_user_id);
				$senders_names[] = $sender -> first_name . " " . $sender -> last_name;
				$i++;
			} else if ($message -> from_user_type == 1) {
				$sender = Teacher::model() -> find("id = " . $message -> from_user_id);
				$senders_names[] = $sender -> first_name . " " . $sender -> last_name;
				$i++;
			} else if ($message -> from_user_type == 2) {
				$sender = Admin::model() -> find("id = " . $message -> from_user_id);
				$senders_names[] = $sender -> first_name . " " . $sender -> last_name;
				$i++;
			}

		}

		//$allMsgs = array();
		$this -> render("inbox", array("allMsgs" => $allMsgs, "senders" => $senders_names));
	}

	public function actionSend() {
		if ($_POST) {
			$transaction = Yii::app() -> db -> beginTransaction();
			try {
				$message = "";
				$msg = new Message;
				$msg -> subject = $_POST['subject'];
				$msg -> content = $_POST['content'];
				$msg -> from_user_id = Yii::app() -> user -> id;
				$date = new DateTime;
				$msg -> date = date('Y-m-d H:i:s');
				if (Yii::app() -> user -> type == "Student")
					$msg -> from_user_type = 0;
				else if (Yii::app() -> user -> type == "Teacher")
					$msg -> from_user_type = 1;
				else if (Yii::app() -> user -> type == "Admin")
					$msg -> from_user_type = 2;

				$receivers = $_POST['receivers'];
				if ($_POST['sender_type'] != 0) {
					if ($_POST['sender_type'] == 5) {
						$receivers_users = Users::model() -> findAll();
					} else {
						$receivers_users = Users::model() -> findAll("type = :type", array(":type" => ($_POST['sender_type'] - 1)));
					}
					$receivers = "";
					foreach ($receivers_users as $user) {
						$receivers .= $user -> username . ",";
					}
					$receivers = substr($receivers, 0, strlen($receivers) - 1);
				}
				if ($msg -> save()) {
					echo $receivers;
					$receivers_array = explode(',', $receivers);
					foreach ($receivers_array as $receiver) {
						$receiverUser = Users::model() -> find("username = :uname", array(":uname" => $receiver));
						if (count($receiverUser) > 0 && $receiverUser -> type == 0) {
							$user = Student::model() -> find("id = :id", array(":id" => $receiverUser -> profile_id));
							$msg_bridge = new MessageReceivers;
							$msg_bridge -> message_id = $msg -> id;
							$msg_bridge -> to_user_id = $user -> id;
							$msg_bridge -> to_user_type = $receiverUser -> type;
							$msg_bridge -> is_read = 0;
							$msg_bridge -> is_hide = 0;
							$msg_bridge -> is_favourite = 0;
							$msg_bridge -> is_important = 0;
							$msg_bridge -> save();
						} else if (count($receiverUser) > 0 && $receiverUser -> type == 1) {
							$user = Teacher::model() -> find("id = :id", array(":id" => $receiverUser -> profile_id));
							$msg_bridge = new MessageReceivers;
							$msg_bridge -> message_id = $msg -> id;
							$msg_bridge -> to_user_id = $user -> id;
							$msg_bridge -> to_user_type = $receiverUser -> type;
							$msg_bridge -> is_read = 0;
							$msg_bridge -> is_hide = 0;
							$msg_bridge -> is_favourite = 0;
							$msg_bridge -> is_important = 0;
							$msg_bridge -> save();
						}
						if (count($receiverUser) > 0 && $receiverUser -> type == 2) {
							$user = Admin::model() -> find("id = :id", array(":id" => $receiverUser -> profile_id));
							$msg_bridge = new MessageReceivers;
							$msg_bridge -> message_id = $msg -> id;
							$msg_bridge -> to_user_id = $user -> id;
							$msg_bridge -> to_user_type = $receiverUser -> type;
							$msg_bridge -> is_read = 0;
							$msg_bridge -> is_hide = 0;
							$msg_bridge -> is_favourite = 0;
							$msg_bridge -> is_important = 0;
							$msg_bridge -> save();
						}

					}
				} else {
					foreach ($msg->getErrors() as $key => $value) {
						foreach ($value as $error) {
							$message .= $error . " ";
						}
					}

				}

				$message = "Send Successfully";
				$transaction -> commit();

			} catch (Exception $e) {

				$transaction -> rollBack();
				$message = "DB problem occured";
			}

			$this -> redirect(array('MailBox/inbox', 'message' => $message));

		} else
			$this -> render("send");

	}

}

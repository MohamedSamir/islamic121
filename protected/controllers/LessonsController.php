<?php

class LessonsController extends Controller {
	
	public function actionGetLessons(){
		$from = $_GET['from'];
		$to = $_GET['to'];
		$data = array();
		$data = array();
		$lessons = Lesson::model() -> findAll("expected_start_time between :start and :end", 
		array(":start" => $from,":end" => $to));
		
		
		foreach($lessons as $lesson){
			$teacher = DataModuleController::getTeacher($lesson->teacher_id);
			$student = DataModuleController::getStudent($lesson->student_id);
			$lessonType = LessonsController::getType($lesson->lesson_type_id);
			$entry = array();
			$entry[] = $student->first_name . " " . $student->last_name;
			$entry[] = $teacher->first_name . " " . $teacher->last_name;
			$entry[] = $lessonType->name;
			$entry[] = $lesson->expected_start_time;
			$entry[] = $lesson->expected_end_time;
			if($lesson->actual_start_time == $lesson->actual_end_time){
				$entry[] = "Not Started";
			}
			else if($lesson->actual_start_time > $lesson->actual_end_time){
				$entry[] = "In Progress";
			}
			else if($lesson->actual_start_time < $lesson->actual_end_time){
				$entry[] = "Finished";
			}
			$entry[] = $lesson->id;
			$data[] = $entry;			
		}
		
		echo json_encode($data);
		
	}	
	
	
	public static function getLessonsToday(){
		$date = getdate();
		$start_date = $date['year'] . "-" . $date['mon'] . "-" . $date['mday'] . " " . "00:00:00";
		
		$end_date =  $date['year'] . "-" . $date['mon'] . "-" . $date['mday'] . " " . "23:59:59";
		
		$lessons = Lesson::model()->find("expected_start_time between :from and :to",array(":from"=>$start_date,":to"=>$end_date));
		return $lessons;
	}
	
	public static function getAllTeachers(){
		$teachers = Teacher::model()->findAll();
		return $teachers;
	}
	
	public static function getAllStudents(){
		$students = Student::model()->findAll();
		return $students;
	}
	
	public static function getAllLessonTypes(){
		$types = LessonType::model()->findAll();
		return $types;
	}
	
	public static function getAllCurrencies(){
		$currency = Currency::model()->findAll();
		return $currency;
	}
	
	public static function getAllSurah(){
		$surah = Surah::model()->findAll();
		return $surah;
	}
	
	public static function from24to12($time){
		$new_time = date("g:i a", strtotime($time));
		return $new_time; 
	}
	
	public static function from12to24($time){
		$new_time = date("H:i", strtotime($time));
		return $new_time; 
	}
	
	
	public function actionLessonState(){
		
		$lesson = Lesson::model()->find("id = :id",array(":id"=>$_GET['id']));
		if($lesson->actual_start_time == $lesson->actual_end_time)
			$this->render("start_lesson",array("lesson"=>$lesson));
		else if($lesson->actual_start_time > $lesson->actual_end_time)
			$this->render("finish_lesson",array("lesson"=>$lesson));
		else if($lesson->actual_start_time < $lesson->actual_end_time)
			$this->render("update_lesson",array("lesson"=>$lesson));
	}
	
	public function actionUpdateLesson(){
			$lesson = Lesson::model()->find("id = :id",array(":id"=>$_POST['id']));
					
			if($_POST){
				$expected_from = date("H:i", strtotime($_POST['from'])); 
				$expected_to = date("H:i", strtotime($_POST['to']));
				$actual_from = date("H:i", strtotime($_POST['actual_from'])); 
				$actual_to = date("H:i", strtotime($_POST['actual_to']));
				
				$expected_start_time = $_POST['date']." ".$expected_from;
				$expected_end_time = $_POST['date']." ".$expected_to;
				$actual_start_time = $_POST['date']." ".$actual_from;
				$actual_end_time = $_POST['date']." ".$actual_to;
				
				$lesson->attributes = $_POST['Lesson'];
				$lesson->expected_start_time = $expected_start_time;
				$lesson->expected_end_time = $expected_end_time;
				$lesson->actual_start_time = $actual_start_time;
				$lesson->actual_end_time = $actual_end_time;
				
				$message = "Lesson updated successfully";
				if(!$lesson->save()){
					$message = "Lesson updated failed";
				}
				$this->render("update_lesson",array("lesson"=>$lesson , "my_message"=>$message));
			
			}
			else $this->render("update_lesson",array("lesson"=>$lesson));
			
			
			
	}
	
	public function actionChangeLessonState(){
		$lesson = Lesson::model()->find("id = :id",array(":id"=>$_POST['id']));
		$date = getdate();
		
		$time = $date['year'] . "-" . sprintf("%02d", $date['mon']) . "-" . sprintf("%02d",$date['mday']) . " " . 
		sprintf("%02d",$date['hours']).":".sprintf("%02d",$date['minutes']).":".sprintf("%02d",$date['seconds']);
		
		
		if($lesson->actual_start_time == $lesson->actual_end_time)
			$lesson->actual_start_time = $time;
		else if ($lesson->actual_start_time > $lesson->actual_end_time)
			$lesson->actual_end_time = $time;
			
		$lesson->save();
		
		$start_date = $date['year'] . "-" . $date['mon'] . "-" . $date['mday'] . " " . "00:00:00";
		$temp_date = strtotime("+14 day", strtotime($start_date));
		$end_date =  date("Y-m-d", $temp_date);
		//$end_date = $date['year'] . "-" . $date['mon'] . "-" . (14 + $date['mday']) . " " . "00:00:00";
		
		
		if(Yii::app()->user->type == "Admin")
			$lessons = Lesson::model() -> findAll("expected_start_time between :start and :end", 
			array(":start" => $start_date,":end" => $end_date));
		else if(Yii::app()->user->type == "Teacher")
			$lessons = Lesson::model() -> findAll("teacher_id = :id and expected_start_time between :start and :end", 
			array(":id"=>Yii::app()->user->id,":start" => $start_date,":end" => $end_date));
		else return;	
		
		if($lesson->actual_start_time > $lesson->actual_end_time)
			$this->render("all_lessons",array("lessons"=>$lessons,"start_date"=>$start_date,"end_date"=>$end_date));
		else if($lesson->actual_start_time < $lesson->actual_end_time){
			$this->render("update_lesson",array("lesson"=>$lesson));
		}
	}
	
	public function actionIndex() {
		$date = getdate();
		$start_date = $date['year'] . "-" . $date['mon'] . "-" . $date['mday'] . " " . "00:00:00";
		$temp_date = strtotime("+14 day", strtotime($start_date));
		$end_date =  date("Y-m-d", $temp_date). " " . "00:00:00";
		if(Yii::app()->user->type == "Admin")
			$lessons = Lesson::model() -> findAll("expected_start_time between :start and :end", 
			array(":start" => $start_date,":end" => $end_date));
		else if(Yii::app()->user->type == "Teacher")
			$lessons = Lesson::model() -> findAll("teacher_id = :id and expected_start_time between :start and :end", 
			array(":id"=>Yii::app()->user->id,":start" => $start_date,":end" => $end_date));
		else return;
		
		$this->render("all_lessons",array("lessons"=>$lessons,"start_date"=>$start_date,"end_date"=>$end_date));
	}
	
	public static function getType($id){
		$lessonType = LessonType::model()->find("id = :id",array(":id"=>$id));
		return $lessonType;
	}

}

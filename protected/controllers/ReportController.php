<?php

class ReportController extends Controller {

	public function actionGetAllStudentsBillingReport() {
		$from = $_GET['from']." 00:00:00";
		$to = $_GET['to']." 23:59:59";
		$result = array();
		$result["data"] = array();

		$accepted_requests = MembersController::getAcceptedStudents();
		foreach ($accepted_requests as $request) {
			$report_data = array();
			$records = TreasuryPaper::model() -> findAll("user_id = :id and type = 0 and date >=  :from ORDER BY date", array(":id" => $request -> student -> id, ":from" => $from));
			$total = Users::model() -> find("profile_id = :id and type = 0", array(":id" => $request -> student -> id));
			if (count($total) == 0) {
				echo "{\"data\":[]}";
				return;
			}
			$start_balance = $total -> value;
			
			$paid_value = 0;
			for ($i = count($records) - 1; $i >= 0; $i--) {
				$start_balance = $start_balance + (-1 * $records[$i] -> paper_type * $records[$i] -> value);
				if($records[$i]->date <= $to){
					$paid_value = $paid_value + ($records[$i] -> paper_type * $records[$i] -> value);
				}
			}
			$lessons = Lesson::model() -> findAll("student_id = :id and actual_start_time between :from and :to ORDER BY actual_start_time", array(":id" => $request -> student -> id, ":from" => $from, ":to" => $to));
			$total_time = 0;
			$total_cost = 0;
			foreach ($lessons as $lesson) {
				$entry = array();
				$from_date = new DateTime($lesson -> expected_start_time);
				$to_date = new DateTime($lesson -> expected_end_time);
				$difference = $from_date -> diff($to_date);
				$period = $difference -> i;
				$total_time += $period;
				$total_cost += $lesson -> cost * ($period / 60.0);

			}
			$entry = array();
			$entry[] = $request->student->first_name . " " . $request->student->last_name;
			$entry[] = ($total_time/60.0);
			$entry[] = $start_balance;
			$entry[] = DataModuleController::getCurrency($request->currency_id);
			$entry[] = $total_cost;
			$entry[] = $paid_value;
			$entry[] = $start_balance + $paid_value - $total_cost;
			$result["data"][] = $entry;
		}

		echo json_encode($result);

	}

	public function actionGetStudentBillingReport() {
		/*$data = array();
		 $appendArray = array();
		 $appendArray["from"] = "dskldksld";
		 $appendArray["to"] = "dskldksld";
		 $appendArray["notes"] = "notes";
		 $data["abc"][] = $appendArray;
		 $data["abc"][] = $appendArray;
		 $data["xyz"][] = $appendArray;
		 var_dump($data);*/

		$from = $_GET['from']." 00:00:00";
		$to = $_GET['to']." 23:59:59";
		$user_id = $_GET['id'];
		$report_data = array();
		$records = TreasuryPaper::model() -> findAll("user_id = :id and type = 0 and date >=  :from ORDER BY date", array(":id" => $user_id, ":from" => $from));
		$total = Users::model() -> find("profile_id = :id and type = 0", array(":id" => $user_id));
		if (count($total) == 0) {
			echo "{\"data\":[]}";
			return;
		}
		$currentTotal = $total -> value;
		$result = array();
		$result["data"] = array();
		$totals = array();
		for ($i = count($records) - 1; $i >= 0; $i--) {
			$currentTotal = $currentTotal + (-1 * $records[$i] -> paper_type * $records[$i] -> value);
			//array_unshift($totals,$currentTotal);
			if ($records[$i] -> date <= $to) {
				$entry = array();
				if ($records[$i] -> paper_type == 1) {
					$entry["in"] = $records[$i] -> value;
					$entry["out"] = 0;
				} else if ($records[$i] -> paper_type == -1) {
					$entry["out"] = $records[$i] -> value;
					$entry["in"] = 0;
				}
				$entry["notes"] = $records[$i] -> notes;
				$entry["hours"] = "-";
				$report_data[$records[$i] -> date][] = $entry;
			}
		}
		$lessons = Lesson::model() -> findAll("student_id = :id and actual_start_time between :from and :to ORDER BY actual_start_time", array(":id" => $user_id, ":from" => $from, ":to" => $to));

		foreach ($lessons as $lesson) {
			$entry = array();
			$from_date = new DateTime($lesson -> expected_start_time);
			$to_date = new DateTime($lesson -> expected_end_time);

			$difference = $from_date -> diff($to_date);
			$period = $difference -> i;

			$lesson_from = substr($lesson -> actual_start_time, strpos($lesson -> actual_start_time, " ") + 1);
			$lesson_to = substr($lesson -> actual_end_time, strpos($lesson -> actual_end_time, " ") + 1);

			$lesson_from = LessonsController::from24to12($lesson_from);
			$lesson_to = LessonsController::from24to12($lesson_to);

			$entry["in"] = 0;
			$entry["out"] = $lesson -> cost * ($period / 60.0);
			$entry["notes"] = "Quran Lesson from " . $lesson_from . " to " . $lesson_to;
			$entry["hours"] = ($period / 60.0);
			$date = substr($lesson -> actual_start_time, 0, strpos($lesson -> actual_start_time, " "));
			$report_data[$date][] = $entry;
		}
		ksort($report_data);
		/*echo $currentTotal;
		 var_dump($report_data);*/
		$period_start_value_entry = array();
		$period_start_value_entry[] = "-";
		$period_start_value_entry[] = "Period Start Value";
		$period_start_value_entry[] = "-";
		$period_start_value_entry[] = "-";
		$period_start_value_entry[] = "-";
		$period_start_value_entry[] = $currentTotal;
		$result["data"][] = $period_start_value_entry;

		$current = $currentTotal;

		foreach ($report_data as $date => $data) {

			foreach ($data as $record) {

				$current += ($record["in"] - $record["out"]);
				$entry = array();
				$entry[] = $date;
				$entry[] = $record["notes"];
				$entry[] = $record["hours"];
				$entry[] = $record["out"];
				$entry[] = $record["in"];
				$entry[] = $current;
				$result["data"][] = $entry;

			}
		}

		echo json_encode($result);

	}

	public function actionStudentBillingReport() {
		$date = getdate();
		$start_date = $date['year'] . "-" . sprintf("%02d", $date['mon']) . "-" . sprintf("%02d", $date['mday']);
		$temp_date = strtotime("next month", strtotime($start_date));
		$end_date = date("Y-m-d", $temp_date);

		//echo $end_date;
		$this -> render("student_billing_report", array("from" => $start_date, "to" => $end_date));

	}
	
	public function actionAllStudentsBillingReport() {
		$date = getdate();
		$start_date = $date['year'] . "-" . sprintf("%02d", $date['mon']) . "-" . sprintf("%02d", $date['mday']);
		$temp_date = strtotime("next month", strtotime($start_date));
		$end_date = date("Y-m-d", $temp_date);

		//echo $end_date;
		$this -> render("all_students_billing_report", array("from" => $start_date, "to" => $end_date));

	}
	
	

	public function actionProgressReport() {
		$date = getdate();

		$from_time = $date['year'] . "-" . sprintf("%02d", $date['mon']) . "-" . sprintf("%02d", $date['mday']);
		$temp_date = strtotime("+1 day", strtotime($from_time));
		$to_time = date("Y-m-d", $temp_date);

		//$to_time = $date['year'] . "-" . sprintf("%02d", $date['mon']) . "-" . sprintf("%02d", $date['mday'] + 1);

		$students = Student::model() -> findAll();

		$this -> render("progress_report", array("from" => $from_time, "to" => $to_time, "students" => $students));
	}

	public function actionGetLessons() {
		$from = $_GET['from'];
		$to = $_GET['to'];
		$student_id = $_GET['id'];
		$data["aaData"] = array();
		$lessons = Lesson::model() -> findAll("student_id = :id and expected_start_time between :start and :end", array(":id" => $student_id, ":start" => $from, ":end" => $to));
		foreach ($lessons as $lesson) {
			$entry = array();
			$entry["date"] = $lesson -> expected_start_time;
			$entry["hefz_from"] = "Surah " . $lesson -> actual_start_surah . ", Ayah " . $lesson -> actual_start_ayah;
			$entry["hefz_to"] = "Surah " . $lesson -> actual_end_surah . ", Ayah " . $lesson -> actual_end_ayah;
			$entry["front_from"] = "Surah " . $lesson -> front_revision_start_surah . ", Ayah " . $lesson -> front_revision_start_ayah;
			$entry["front_to"] = "Surah " . $lesson -> front_revision_end_surah . ", Ayah " . $lesson -> front_revision_end_ayah;
			$entry["back_from"] = "Surah " . $lesson -> back_revision_start_surah . ", Ayah " . $lesson -> back_revision_start_ayah;
			$entry["back_to"] = "Surah " . $lesson -> back_revision_end_surah . ", Ayah " . $lesson -> back_revision_end_ayah;
			$entry["grade"] = $lesson -> grade;
			$entry["notes"] = $lesson -> notes;
			$data["aaData"][] = $entry;
		}

		echo json_encode($data);

	}

}

   <?php if(isset($my_message)): ?>

	<script>
		$(document).ready(function(){

			noty({
				text: '<?php echo $my_message ?>
				',
				layout: 'topRight',
				type: 'error',
				timeout: 5000,

			});
		});
	</script>
<?php endif; ?>
           
                <?php $teachers = LessonsController::getAllTeachers(); 
                	  $students = LessonsController::getAllStudents();
					  $types =  LessonsController::getAllLessonTypes();
					  $currencies = LessonsController::getAllCurrencies();
					  $surah = LessonsController::getAllSurah(); ?>
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal" action="index.php?r=lessons/updateLesson" method="post">
                            <input type="hidden" name="id" value="<?php echo $lesson->id; ?>" />
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title">Lesson View <strong>Preview</strong></h3>
                                </div>
                                <div class="panel-body">
		
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Teacher Name</label>
                                        <div class="col-md-6">                                                                                
                                            <select name="Lesson[teacher_id]" class="form-control select" data-live-search="true" style="display: none;">
                                                <?php foreach ($teachers as $teacher): ?>
                                                	<?php if($teacher->id == $lesson->teacher_id): ?>
                                                		<option value="<?php echo $lesson->teacher_id ?>" selected><?php echo $teacher->first_name . " " . $teacher->last_name; ?></option>
                                                	<?php else: ?>
                                                		<option value="<?php echo $lesson->teacher_id ?>"><?php echo $teacher->first_name . " " . $teacher->last_name; ?></option>
                                            	<?php endif; ?>
                                            	<?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Student Name</label>
                                        <div class="col-md-6">                                                                                
                                            <select name="Lesson[student_id]" class="form-control select" data-live-search="true" style="display: none;">
                                                <?php foreach ($students as $student): ?>
                                                	<?php if($student->id == $lesson->student_id): ?>
                                                		<option value="<?php echo $lesson->student_id ?>" selected><?php echo $student->first_name . " " . $student->last_name; ?></option>
                                                	<?php else: ?>
                                                		<option value="<?php echo $lesson->student_id ?>"><?php echo $student->first_name . " " . $student->last_name; ?></option>
                                            	<?php endif; ?>
                                            	<?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Lesson Type</label>
                                        <div class="col-md-6 col-xs-12">                                                                                
                                            <select name="Lesson[lesson_type_id]" class="form-control select" data-live-search="true" style="display: none;">
                                                <?php foreach ($types as $type): ?>
                                                	<?php if($type->id == $lesson->lesson_type_id): ?>
                                                		<option value="<?php echo $lesson->lesson_type_id ?>" selected><?php echo $type->name; ?></option>
                                                	<?php else: ?>
                                                		<option value="<?php echo $lesson->lesson_type_id ?>"><?php echo $type->name; ?></option>
                                            	<?php endif; ?>
                                            	<?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Date</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input name="date" type="text" class="form-control datepicker" value="<?php echo substr($lesson->expected_start_time,0,strpos($lesson->expected_start_time, " ")); ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Expected Time</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon">From</span>
                                                <div class="input-group bootstrap-timepicker">
                                                    <input name="from" type="text" class="form-control timepicker" value="<?php echo LessonsController::from24to12(substr($lesson->expected_start_time,strpos($lesson->expected_start_time, " ")+1)); ?>">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon">To</span>
                                                <div class="input-group bootstrap-timepicker">
                                                    <input name="to" type="text" class="form-control timepicker" value="<?php echo LessonsController::from24to12(substr($lesson->expected_end_time,strpos($lesson->expected_end_time, " ")+1)); ?>">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Actual Time</label>
                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon">From</span>
                                                <div class="input-group bootstrap-timepicker">
                                                    <input name="actual_from" type="text" class="form-control timepicker" value="<?php echo LessonsController::from24to12(substr($lesson->actual_start_time,strpos($lesson->actual_start_time, " ")+1)); ?>">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon">To</span>
                                                <div class="input-group bootstrap-timepicker">
                                                    <input name="actual_to" type="text" class="form-control timepicker" value="<?php echo LessonsController::from24to12(substr($lesson->actual_end_time,strpos($lesson->actual_end_time, " ")+1)); ?>">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Price</label>

                                        <div class="col-md-4 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-money"></span></span>
                                                <input name="cost" type="number" class="form-control" value="<?php echo $lesson->cost ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-xs-12">
                                            <select name="Lesson[currency_id]" class="form-control select" style="display: none;">
                                                <?php foreach($currencies as $currency): ?>
                                                	<?php if($currency->id == $lesson->currency_id): ?>
                                                		<option value="<?php echo $currency->id ?>" selected><?php echo $currency->name ?></option>
                                                	<?php else: ?>
                                                		<option value="<?php echo $currency->id ?>" ><?php echo $currency->name ?></option>
                                                	<?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Quran Lesson Actual Start</label>

                                        <div class="col-md-4 col-xs-12">
                                            <select name="Lesson[actual_start_surah]" class="form-control select" data-live-search="true" style="display: none;">
                                                <?php foreach($surah as $sur): ?>
                                                	<?php if($lesson->actual_start_surah == $sur->id): ?>
                                                		<option value="<?php echo $sur->id; ?>" selected><?php echo $sur->name; ?></option>
                                                	<?php else: ?>
                                                		<option value="<?php echo $sur->id; ?>"><?php echo $sur->name; ?></option>
                                                	<?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-md-2 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon">Ayah</span>
                                                <input name="Lesson[actual_start_ayah]" type="number" class="form-control" value="<?php echo $lesson->actual_start_ayah; ?>">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Quran Lesson Actual End</label>

										<div class="col-md-4 col-xs-12">
                                            <select name="Lesson[actual_end_surah]" class="form-control select" data-live-search="true" style="display: none;">
                                                <?php foreach($surah as $sur): ?>
                                                	<?php if($lesson->actual_end_surah == $sur->id): ?>
                                                		<option value="<?php echo $sur->id; ?>" selected><?php echo $sur->name; ?></option>
                                                	<?php else: ?>
                                                		<option value="<?php echo $sur->id; ?>"><?php echo $sur->name; ?></option>
                                                	<?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-md-2 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon">Ayah</span>
                                                <input name="Lesson[actual_end_ayah]" type="number" class="form-control" value="<?php echo $lesson->actual_end_ayah; ?>">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Quran Lesson Front-Revision Start</label>

                                        <div class="col-md-4 col-xs-12">
                                            <select name="Lesson[front_revision_start_surah]" class="form-control select" data-live-search="true" style="display: none;">
                                                <?php foreach($surah as $sur): ?>
                                                	<?php if($lesson->front_revision_start_surah == $sur->id): ?>
                                                		<option value="<?php echo $sur->id; ?>" selected><?php echo $sur->name; ?></option>
                                                	<?php else: ?>
                                                		<option value="<?php echo $sur->id; ?>"><?php echo $sur->name; ?></option>
                                               		<?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-md-2 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon">Ayah</span>
                                                <input name="Lesson[front_revision_start_ayah]" type="number" class="form-control" value="<?php echo $lesson->front_revision_start_ayah; ?>">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Quran Lesson Front-Revision End</label>

                                        <div class="col-md-4 col-xs-12">
                                            <select name="Lesson[front_revision_end_surah]" class="form-control select" data-live-search="true" style="display: none;">
                                                <?php foreach($surah as $sur): ?>
                                                	<?php if($lesson->front_revision_end_surah == $sur->id): ?>
                                                		<option value="<?php echo $sur->id; ?>" selected><?php echo $sur->name; ?></option>
                                                	<?php else: ?>
                                                		<option value="<?php echo $sur->id; ?>"><?php echo $sur->name; ?></option>
                                               		<?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-md-2 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon">Ayah</span>
                                                <input name="Lesson[front_revision_end_ayah]" type="number" class="form-control" value="<?php echo $lesson->front_revision_end_ayah; ?>">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Quran Lesson Back-Revision Start</label>

                                        <div class="col-md-4 col-xs-12">
                                            <select name="Lesson[back_revision_start_surah]" class="form-control select" data-live-search="true" style="display: none;">
                                                <?php foreach($surah as $sur): ?>
                                                	<?php if($lesson->back_revision_start_surah == $sur->id): ?>
                                                		<option value="<?php echo $sur->id; ?>" selected><?php echo $sur->name; ?></option>
                                                	<?php else: ?>
                                                		<option value="<?php echo $sur->id; ?>"><?php echo $sur->name; ?></option>
                                               		<?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-md-2 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon">Ayah</span>
                                                <input name="Lesson[back_revision_start_ayah]" type="number" class="form-control" value="<?php echo $lesson->back_revision_start_ayah ?>">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Quran Lesson Back-Revision End</label>

                                        <div class="col-md-4 col-xs-12">
                                            <select name="Lesson[back_revision_end_surah]" class="form-control select" data-live-search="true" style="display: none;">
                                                <?php foreach($surah as $sur): ?>
                                                	<?php if($lesson->back_revision_end_surah == $sur->id): ?>
                                                		<option value="<?php echo $sur->id; ?>" selected><?php echo $sur->name; ?></option>
                                                	<?php else: ?>
                                                		<option value="<?php echo $sur->id; ?>"><?php echo $sur->name; ?></option>
                                               		<?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-md-2 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon">Ayah</span>
                                                <input name="Lesson[back_revision_end_ayah]" type="number" class="form-control" value="<?php echo $lesson->back_revision_end_ayah ?>">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Grade</label>

                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-sort-numeric-asc"></span></span>
                                                <input name="Lesson[grade]" type="number" class="form-control" value="<?php echo $lesson->grade; ?>">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Notes</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <textarea name="Lesson[notes]" class="form-control" rows="3"><?php echo $lesson->notes; ?></textarea>
                                        </div>
                                    </div>


                                </div>

                                <div class="panel-footer">
                                    <button type="reset"  class="btn btn-danger">Reset</button>                        
                                    <button type="submit" class="btn btn-primary pull-right">Save Changes</button>
                                </div>

                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->   
                <!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="js/plugins/icheck/icheck.min.js"></script>
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
        
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>

<script type="text/javascript" src="js/plugins/bootstrap/jquery.tagsinput.min.js"></script>
<!-- END THIS PAGE PLUGINS -->
                                             

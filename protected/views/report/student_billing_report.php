<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Student Billing Report</h3>
				</div>

				<div class="panel-body">

					<form action="#" class="form-horizontal">
						<div class="form-group">

							<label class="col-md-1 control-label">Student</label>
							<div class="col-md-3">
								<?php if(Yii::app()->user->type  == "Admin"): ?>
									<select id="uid" class="form-control select" data-live-search="true">
								<?php else: ?>
									<select id="uid" class="form-control select" data-live-search="true" disabled="">
								<?php endif; ?>
								<?php if(Yii::app()->user->type != "Admin"): ?>
									<option value="<?php echo Yii::app()->user->id ?>"><?php echo Yii::app()->user->first_name . " ". Yii::app()->user->last_name; ?></option>
								<?php else: ?>
									<?php $accepted_students = MembersController::getAcceptedStudents(); ?>
									<?php foreach ($accepted_students as $student): ?>
										<option value="<?php echo $student->student->id ?>"><?php echo $student->student->first_name . " ". $student->student->last_name; ?></option>
									<?php endforeach; ?>
								<?php endif; ?>
									
								</select>
							</div>

							<label class="col-md-1 control-label">From</label>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
									<input id="from" type="text" class="form-control datepicker" value="<?php echo $from ?>">
								</div>
							</div>

							<label class="col-md-1 control-label">To</label>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
									<input id="to" type="text" class="form-control datepicker" value="<?php echo $to ?>">
								</div>
							</div>

						</div>
					</form>

				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">

				<div class="panel-body">
					<div class="table-responsive">
						<table id="billing" class="table datatable">
							<thead>
								<tr>
									<th width="20%" style="text-align: center;">Date</th>
									<th width="30%" style="text-align: center;">Notes</th>
									<th width="10%" style="text-align: center;">Hours</th>
									<th width="10%" style="text-align: center;">Out</th>
									<th width="10%" style="text-align: center;">In</th>
									<th width="20%" style="text-align: center;">Total</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- START THIS PAGE PLUGINS-->        
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>        
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="js/plugins/scrolltotop/scrolltopcontrol.js"></script>

<script type="text/javascript" src="js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="js/plugins/morris/morris.min.js"></script>       
<script type="text/javascript" src="js/plugins/rickshaw/d3.v3.js"></script>
<script type="text/javascript" src="js/plugins/rickshaw/rickshaw.min.js"></script>
<script type='text/javascript' src='js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
<script type='text/javascript' src='js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
<script type='text/javascript' src='js/plugins/bootstrap/bootstrap-datepicker.js'></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>                
<script type="text/javascript" src="js/plugins/owl/owl.carousel.min.js"></script>                 

<script type="text/javascript" src="js/plugins/moment.min.js"></script>
<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/student_billing_report.js"></script>


<!-- END THIS PAGE PLUGINS-->




           <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">


             <div class="row">
                <div class="col-md-12">

                    <!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Progress Report</h3>
                        </div>


                        <div class="panel-body">

                            <form action="#" class="form-horizontal">
                            	
                                <div class="form-group">

                                    <label class="col-md-1 control-label">Student</label>
                                    <div class="col-md-3">                                                                                
                                        <select id="student" class="form-control select" data-live-search="true" style="display: none;">
                                            <?php foreach ($students as $student): ?>
                                            	<option value="<?php echo $student->id ?>"><?php echo $student->first_name . " " . $student->last_name; ?></option>
                                        	<?php endforeach; ?>
                                        </select>
                                    </div>

                                    <label class="col-md-1 control-label">From</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input id="from" type="text" class="form-control datepicker" value="<?php echo $from; ?>">
                                        </div>
                                    </div>

                                    <label class="col-md-1 control-label">To</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input id="to" type="text" class="form-control datepicker" value="<?php echo $to; ?>">
                                        </div>
                                    </div>

                                </div>
                            </form>

                            
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="datatable" class="table datatable">
                                    <thead>
                                        <tr>
                                            <th colspan="1" width="10%" style="text-align: center;"></th>
                                            <th colspan="2" width="20%" style="text-align: center;">Quran Hefz</th>
                                            <th colspan="2" width="20%" style="text-align: center;">Front Revision</th>
                                            <th colspan="2" width="20%" style="text-align: center;">Back Revision</th>
                                            <th colspan="1" width="10%" style="text-align: center;"></th>
                                            <th colspan="1" width="20%" style="text-align: center;"></th>
                                        </tr>
                                        <tr>
                                            <th width="10%" style="text-align: center;">Date</th>
                                            <th width="10%" style="text-align: center;">From</th>
                                            <th width="10%" style="text-align: center;">To</th>
                                            <th width="10%" style="text-align: center;">From</th>
                                            <th width="10%" style="text-align: center;">To</th>
                                            <th width="10%" style="text-align: center;">From</th>
                                            <th width="10%" style="text-align: center;">To</th>
                                            <th width="10%" style="text-align: center;">Grade</th>
                                            <th width="20%" style="text-align: center;">Notes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END SIMPLE DATATABLE -->

                </div>
            </div>

        </div>
        <!-- END PAGE CONTENT WRAPPER -->                                
        
<!-- START THIS PAGE PLUGINS-->        
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>        
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="js/plugins/scrolltotop/scrolltopcontrol.js"></script>

<script type="text/javascript" src="js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="js/plugins/morris/morris.min.js"></script>       
<script type="text/javascript" src="js/plugins/rickshaw/d3.v3.js"></script>
<script type="text/javascript" src="js/plugins/rickshaw/rickshaw.min.js"></script>
<script type='text/javascript' src='js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
<script type='text/javascript' src='js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
<script type='text/javascript' src='js/plugins/bootstrap/bootstrap-datepicker.js'></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>                
<script type="text/javascript" src="js/plugins/owl/owl.carousel.min.js"></script>                 

<script type="text/javascript" src="js/plugins/moment.min.js"></script>
<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/progress_report.js"></script>
<!-- END THIS PAGE PLUGINS-->

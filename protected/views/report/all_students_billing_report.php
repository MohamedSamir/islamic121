<!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">


               <div class="row">
                <div class="col-md-12">

                    <!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">All Students Billing Report</h3>
                        </div>


                        <div class="panel-body">

                            <form action="#" class="form-horizontal">
                                <div class="form-group">

                                <div class="col-md-2"></div>

                                    <label class="col-md-1 control-label">From</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input id="from" type="text" class="form-control datepicker" value="<?php echo $from ?>">
                                        </div>
                                    </div>

                                    <label class="col-md-1 control-label">To</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input id="to" type="text" class="form-control datepicker" value="<?php echo $to ?>">
                                        </div>
                                    </div>

                                </div>
                            </form>

                            
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="billing" class="table datatable">
                                    <thead>
                                        <tr>
                                            <th width="30%" style="text-align: center;">Student</th>
                                            <th width="10%" style="text-align: center;">Total Hours</th>
                                            <th width="10%" style="text-align: center;">Start Balance</th>
                                            <th width="20%" style="text-align: center;">Currency</th>
                                            <th width="10%" style="text-align: center;">Hours Cost</th>
                                            <th width="10%" style="text-align: center;">Paid Value</th>
                                            <th width="10%" style="text-align: center;">Total</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END SIMPLE DATATABLE -->

                </div>
            </div>

        </div>
        <!-- END PAGE CONTENT WRAPPER -->

<!-- START THIS PAGE PLUGINS-->        
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>        
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="js/plugins/scrolltotop/scrolltopcontrol.js"></script>

<script type="text/javascript" src="js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="js/plugins/morris/morris.min.js"></script>       
<script type="text/javascript" src="js/plugins/rickshaw/d3.v3.js"></script>
<script type="text/javascript" src="js/plugins/rickshaw/rickshaw.min.js"></script>
<script type='text/javascript' src='js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
<script type='text/javascript' src='js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
<script type='text/javascript' src='js/plugins/bootstrap/bootstrap-datepicker.js'></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>                
<script type="text/javascript" src="js/plugins/owl/owl.carousel.min.js"></script>                 

<script type="text/javascript" src="js/plugins/moment.min.js"></script>
<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/all_students_billing_report.js"></script>
<!-- END THIS PAGE PLUGINS-->

<?php

/**
 * This is the model class for table "message_receivers".
 *
 * The followings are the available columns in table 'message_receivers':
 * @property integer $id
 * @property integer $message_id
 * @property integer $to_user_id
 * @property integer $to_user_type
 * @property integer $is_read
 * @property integer $is_hide
 * @property integer $is_important
 * @property integer $is_favourite
 *
 * The followings are the available model relations:
 * @property Message $message
 */
class MessageReceivers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'message_receivers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message_id, to_user_id, to_user_type', 'required'),
			array('message_id, to_user_id, to_user_type, is_read, is_hide, is_important, is_favourite', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, message_id, to_user_id, to_user_type, is_read, is_hide, is_important, is_favourite', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'message' => array(self::BELONGS_TO, 'Message', 'message_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'message_id' => 'Message',
			'to_user_id' => 'To User',
			'to_user_type' => 'To User Type',
			'is_read' => 'Is Read',
			'is_hide' => 'Is Hide',
			'is_important' => 'Is Important',
			'is_favourite' => 'Is Favourite',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('message_id',$this->message_id);
		$criteria->compare('to_user_id',$this->to_user_id);
		$criteria->compare('to_user_type',$this->to_user_type);
		$criteria->compare('is_read',$this->is_read);
		$criteria->compare('is_hide',$this->is_hide);
		$criteria->compare('is_important',$this->is_important);
		$criteria->compare('is_favourite',$this->is_favourite);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MessageReceivers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
